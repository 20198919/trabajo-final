using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proyectofinal
{
    class Program
    {
        static void Main(string[] args)
        {


            float pago, Interes_pagado, Capital_pagado, Balance, tasa_interes_anual, tasa_interes_mensual;
            int fila, Plazo, i;



            Console.Write("Introduce el monto del prestamo: ");
            float.TryParse(Console.ReadLine(), out Balance);
            Console.Write("Introduce la tasa de interes anual: ");
            float.TryParse(Console.ReadLine(), out tasa_interes_anual);
            Console.Write("Introduce el plazo en meses: ");
            int.TryParse(Console.ReadLine(), out Plazo);



            tasa_interes_mensual = (tasa_interes_anual / 100) / 12;


            pago = tasa_interes_mensual + 1;
            pago = (float)Math.Pow(pago, Plazo);
            pago = pago - 1;
            pago = tasa_interes_mensual / pago;
            pago = tasa_interes_mensual + pago;
            pago = Balance * pago;


            Console.WriteLine();
            fila = 1;
            Console.WriteLine();
            Console.WriteLine();
            Console.Write(" Numero de pago \t");
            Console.Write("Pago \t\t");
            Console.Write("Intereses Pagados \t\t");
            Console.Write("Capital Pagado \t\t");
            Console.Write("Monto del prestamo \t");
            Console.WriteLine();
            Console.WriteLine();
            Console.Write("\t0");
            Console.WriteLine("\t\t\t\t\t\t\t\t\t\t\t{0}", Balance);


            for (i = 1; i <= Plazo; i++)
            {


                Console.Write("\t{0}\t\t", fila);


                Console.Write("{0}\t", pago);


                Interes_pagado = tasa_interes_mensual * Balance;
                Console.Write("{0}\t\t", Interes_pagado);


                Capital_pagado = pago - Interes_pagado;
                Console.Write("\t{0}\t", Capital_pagado);


                Balance = Balance - Capital_pagado;
                Console.Write("\t{0}\t", Balance);

                fila = fila + 1;
                Console.WriteLine();

            }
            Console.ReadLine();
        }
    }
}
